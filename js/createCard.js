import { deleteCard } from './deleteCard.js';
import { badgeCheck, deleteIcon, commentIcon, likeIcon, retweetIcon, shareIcon } from './icons.js';

export function createCard(cardContainer, user, post) {
    const cardElement = document.createElement('div');
    cardElement.className = 'card';
    cardElement.dataset.postId = post.id;

    const cardWrapper = document.createElement('div');
    cardWrapper.className = 'card__wrapper';

    const avatarDiv = document.createElement('div');
    avatarDiv.className = 'card__avatar';

    const photo = document.createElement('img');
    photo.className = 'card__photo';
	photo.src = `https://i.pravatar.cc/50?u=${user.name}`;
	
    avatarDiv.appendChild(photo);
    cardWrapper.appendChild(avatarDiv);

    const postDiv = document.createElement('div');
    postDiv.className = 'card__post';

    const infoDiv = document.createElement('div');
    infoDiv.className = 'card__info';

    const headDiv = document.createElement('div');
    headDiv.className = 'card__head';

    const headerDiv = document.createElement('div');
    headerDiv.className = 'card__header';

    const name = document.createElement('span');
    name.className = 'card__name';
	name.textContent = `${user.name}`;
	
    headerDiv.appendChild(name);

    const badgeCheckSvg = document.createElement('div');
    badgeCheckSvg.className = 'card__badge-check';
	badgeCheckSvg.innerHTML = badgeCheck;
	
    headerDiv.appendChild(badgeCheckSvg);

    const username = document.createElement('span');
	username.className = 'card__username';
	username.textContent = `@${user.username}`;
	
    headerDiv.appendChild(username);

    const timeSpan = document.createElement('span');
	timeSpan.textContent = '·';
	
    headerDiv.appendChild(timeSpan);

    const time = document.createElement('time');
    time.setAttribute('title', '08:17 AM · Feb 12, 2023');
	time.textContent = 'Feb 12';
	
    headerDiv.appendChild(time);
    headDiv.appendChild(headerDiv);

    const deleteButton = document.createElement('div');
    deleteButton.className = 'card__delete';
    deleteButton.setAttribute('title', 'Delete');
	deleteButton.innerHTML = deleteIcon;
	
    headDiv.appendChild(deleteButton);
    infoDiv.appendChild(headDiv);

    const titleDiv = document.createElement('div');
    titleDiv.className = 'card__title';
	titleDiv.innerText = post.title;
	
    infoDiv.appendChild(titleDiv);

    const textDiv = document.createElement('div');
    textDiv.className = 'card__text';
	textDiv.innerText = post.body;
	
    infoDiv.appendChild(textDiv);
    postDiv.appendChild(infoDiv);

    const iconsDiv = document.createElement('div');
    iconsDiv.className = 'card__icons';

    const icon1Div = document.createElement('div');
	icon1Div.className = 'card__icon-wrapper';
	icon1Div.setAttribute('title', 'Reply');

    const icon1Svg = document.createElement('div');
    icon1Svg.className = 'card__icon';
	icon1Svg.innerHTML = commentIcon;
	
    icon1Div.appendChild(icon1Svg);

    const icon1Span = document.createElement('span');
    icon1Span.className = 'card__icon-text';
	icon1Span.textContent = ' 284 ';
	
    icon1Div.appendChild(icon1Span);
    iconsDiv.appendChild(icon1Div);

    const icon2Div = document.createElement('div');
	icon2Div.className = 'card__icon-wrapper';
	icon2Div.setAttribute('title', 'Retweet');

    const icon2Svg = document.createElement('div');
    icon2Svg.className = 'card__icon';
	icon2Svg.innerHTML = retweetIcon;
	
    icon2Div.appendChild(icon2Svg);

    const icon2Span = document.createElement('span');
    icon2Span.className = 'card__icon-text';
	icon2Span.textContent = ' 988 ';
	
    icon2Div.appendChild(icon2Span);
    iconsDiv.appendChild(icon2Div);

    const icon3Div = document.createElement('div');
	icon3Div.className = 'card__icon-wrapper';
	icon3Div.setAttribute('title', 'Like');

    const icon3Svg = document.createElement('div');
    icon3Svg.className = 'card__icon';
	icon3Svg.innerHTML = likeIcon;
	
    icon3Div.appendChild(icon3Svg);

    const icon3Span = document.createElement('span');
    icon3Span.className = 'card__icon-text';
	icon3Span.textContent = ' 10.5 тис. ';
	
    icon3Div.appendChild(icon3Span);
    iconsDiv.appendChild(icon3Div);

    const icon4Div = document.createElement('div');
	icon4Div.className = 'card__icon-wrapper';
	icon4Div.setAttribute('title', 'Share');

    const icon4Svg = document.createElement('div');
	icon4Svg.className = 'card__icon';
	icon4Svg.innerHTML = shareIcon;
	
    icon4Div.appendChild(icon4Svg);
    iconsDiv.appendChild(icon4Div);

	postDiv.appendChild(iconsDiv);
	
	cardWrapper.appendChild(postDiv);
	
    cardElement.appendChild(cardWrapper);

    cardContainer.appendChild(cardElement);

    deleteButton.addEventListener('click', () => {
        deleteCard(cardElement);
    });
}
