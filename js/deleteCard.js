export function deleteCard(cardElement) {
    const postId = cardElement.dataset.postId;

    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE',
    })
        .then(response => {
            if (response.ok) {
                cardElement.remove();
            } else {
                throw new Error('Failed to delete card');
            }
        })
        .catch(error => console.log(error));
}
