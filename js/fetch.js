export function getUsers() {
    return fetch('https://ajax.test-danit.com/api/json/users')
        .then(response => response.json())
        .catch(error => console.log(error));
}

export function getPosts() {
    return fetch('https://ajax.test-danit.com/api/json/posts')
        .then(response => response.json())
        .catch(error => console.log(error));
}

export function deletePost(postId) {
    return fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE',
    }).catch(error => console.log(error));
}
