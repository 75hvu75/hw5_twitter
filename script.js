import Card from './js/card.js';
import { createCard } from './js/createCard.js';
import { deleteCard } from './js/deleteCard.js';
import { getUsers, getPosts, deletePost } from './js/fetch.js';

const root = document.getElementById('root');
const nav = document.querySelector('.nav');

// Відображення публікацій

async function fetchData() {
    try {
        const users = await getUsers();
        const posts = await getPosts();

        posts.forEach(post => {
            const user = users.find(user => user.id === post.userId);
            createCard(root, user, post);
        });
    } catch (error) {
        console.log(error);
    }
}

fetchData();

// Видалення картки при кліку на кнопку

root.addEventListener('click', event => {
    const deleteButton = event.target.closest('.card__delete');
    if (deleteButton) {
        const cardElement = deleteButton.parentNode.parentNode;
        const postId = cardElement.dataset.postId;
        deleteCard(cardElement);
        deletePost(postId)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to delete post');
                }
            })
            .catch(error => console.log(error));
    }
});

//Додавання-видалення класу .active

const toggleUnderline = () => {
    nav.addEventListener('click', e => {
        if (e.target.classList.contains('nav__item')) {
            const selectedItem = e.target;
            const items = nav.querySelectorAll('.nav__item');

            items.forEach(item => {
                item.classList.remove('active');
            });

            selectedItem.classList.add('active');
        }
    });
};

toggleUnderline();
